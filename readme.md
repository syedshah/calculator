# PHP Test Project #

Author: Syed Noab Hussain Shah (s.shah@email.com)

Website: http://IDontHaveOne.com

## How To Checkout The Poject ##

    $ git clone https://syedshah@bitbucket.org/syedshah/calculator.git

## How To Setup The Test Environment Setup ##

### Install JDK ###

    http://www.oracle.com/technetwork/java/javaee/downloads/java-ee-sdk-6u3-jdk-6u29-downloads-523388.html

### Install JDK 6 ###
    Set up your JAVA_HOME enviromental variable to point to the JRE

### Install Spring 3.2 ###

    http://www.springsource.org/spring-framework


**NOTE:**

* Update **VERSION** to match the version of php installed on your system.

### Install Ant (comes preinstalled on Mac) ###

    Optional

### Install JUnit ###

    $ sudo pear config-set auto_discover 1
    $ sudo pear config-set preferred_state alpha
    $ sudo pear install pear.phpunit.de/PHPUnit

### Create build.properties File ###

Duplicate build.propterties.dist as build.propterties and update the settings to match your system.

Your system is now ready to run tests and generate code coverage reports using Ant, try the out the following:

    $ cd PROJECT_DIR
    $ ant phpunit

**NOTE:**

* Replace **PROJECT_DIR** with the path to where your project files are.

## PHPUnit Guide ##

PHPUnit needs to be run from the project test folder, so make sure you are in the correct directory by going to:

    $ cd {PROJECT_PATH}/test/php/unit

Replace {**PROJECT_PATH**} with the path to where you have your project files

### Run CalculatorTest ###

    $ phpunit RaguSource/CalculatorTest.php

You should get something like this:

    PHPUnit 3.7.15 by Sebastian Bergmann.

    Configuration read from /Volumes/ragusource/php-test/test/php/unit/phpunit.xml

    ....

    Time: 0 seconds, Memory: 8.25Mb

    OK (4 tests, 12 assertions)

    Generating code coverage report in Clover XML format ... done

    Generating code coverage report in HTML format ... done

### Run All Unit Tests ###

    $ phpunit RaguSource

**NOTE:**

* The above examples run a vanilla test, if more parameters are required to be passed to PHPUnit these can just be added to the command.# This is my README
